const fetch = require("node-fetch");

describe("/currencies", () => {
    it("should return a list of currencies", async () => {
        const response = await fetch("http://localhost/currencies");
        const currencies = await response.json();
        expect(currencies).toHaveLength(3);
    });
});